import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {RestClientService} from "../rest-client.service";

@Component({
  selector: 'app-manager',
  templateUrl: './manager.component.html',
  styleUrls: ['./manager.component.css']
})
export class ManagerComponent implements OnInit {

  constructor(private router:Router,private restclient: RestClientService) { }
 employeelist=[];
    ngOnInit(): void {
    this.restclient.getEmployee().subscribe(res=>{console.log("employee list",res);
    this.employeelist=res.data;
    })
  }
  gotouser(){
    this.router.navigateByUrl('/manager');
  }


}
