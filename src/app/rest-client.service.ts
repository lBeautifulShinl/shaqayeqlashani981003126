import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {observableToBeFn} from "rxjs/internal/testing/TestScheduler";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class RestClientService {

  constructor(private http: HttpClient) {
  }

  getEmployee(): Observable<any> {
    return this.http.get('http://dummy.restapiexample.com/api/v1/employees');
  }

}
